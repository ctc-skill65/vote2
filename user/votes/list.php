<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$now = time();
$items = DB::result("SELECT * FROM `votes`");
ob_start();
?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อเลือกตั้ง</th>
            <th>วันเวลาเริ่มเลือกตั้ง</th>
            <th>วันเวลาสิ้นสุดเลือกตั้ง</th>
            <th>สถานะ</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['vote_id'] ?></td>
                <td><?= $item['vote_name'] ?></td>
                <td><?= $item['start_time'] ?></td>
                <td><?= $item['end_time'] ?></td>
                <td>
                    <?php
                    if ($now > strtotime($item['start_time']) && $now < strtotime($item['end_time'])) {
                        echo 'เปิด';
                    } else {
                        echo 'ปิด';
                    }
                    ?>
                </td>

                <td>
                    <a href="<?= url("/user/votes/detail.php?id={$item['vote_id']}") ?>">
                        เลือกตั้ง
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="">
                        ผลการเลือกตั้ง
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการเลือกตั้ง';
require ROOT . '/admin/layout.php';
