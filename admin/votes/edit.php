<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$vote_id = get('vote');
$page_path = "/admin/votes/edit.php?vote={$vote_id}";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('candidates', "`candidate_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if (post('vote_name')) {
    $result = DB::update('votes', [
        'vote_name' => post('vote_name'),
        'start_time' => post('start_time'),
        'end_time' => post('end_time')
    ], "`vote_id`='{$vote_id}'");
    if ($result) {
        setAlert('success', "แก้ไขเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขเลือกตั้งได้");
    }
    redirect($page_path);
}

if (post('number')) {
    $img = uplode('img', '/storage/candidate_img');
    if (!$img) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพผู้ลงเลือกตั้งได้");
        redirect($page_path);
    }

    $result = DB::insert('candidates', [
        'vote_id' => $vote_id,
        'number' => post('number'),
        'img' => $img,
        'firstname' => post('firstname'),
        'lastname' => post('lastname')
    ]);

    if ($result) {
        setAlert('success', "เพื่มผู้ลงเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพื่มผู้ลงเลือกตั้งได้");
    }
    redirect($page_path);
}

$now = time();
$items = DB::result("SELECT * FROM `candidates` WHERE `vote_id`='{$vote_id}'");
$data = DB::row("SELECT * FROM `votes` WHERE `vote_id`='{$vote_id}'");
ob_start();
?>
<a href="<?= url("/admin/votes/list.php") ?>">
    <button>< กลับ</button>
</a>
<?= showAlert() ?>
<h3>แก้ไขเลือกตั้ง</h3>
<form method="post">
    <label for="vote_name">ชื่อเลือกตั้ง</label>
    <input type="text" name="vote_name" id="vote_name" value="<?= $data['vote_name'] ?>" required>
    <br>

    <label for="start_time">วันเวลาเริ่มเลือกตั้ง</label>
    <input type="datetime-local" name="start_time" id="start_time" value="<?= $data['start_time'] ?>" required>
    <br>

    <label for="end_time">วันเวลาสิ้นสุดเลือกตั้ง</label>
    <input type="datetime-local" name="end_time" id="end_time" value="<?= $data['end_time'] ?>" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<h3>เพื่มผู้ลงเลือกตั้ง</h3>
<form method="post" enctype="multipart/form-data">
    <label for="number">หมายเลขผู้ลงเลือกตั้ง</label>
    <input type="number" name="number" id="number" required>
    <br>

    <label for="img">ภาพผู้ลงเลือกตั้ง</label>
    <input type="file" name="img" id="img" accept="image/*" required>
    <br>

    <label for="firstname">ชื่อผู้ลงเลือกตั้ง</label>
    <input type="text" name="firstname" id="firstname" required>
    <br>

    <label for="lastname">นามสกุลผู้ลงเลือกตั้ง</label>
    <input type="text" name="lastname" id="lastname" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<h3>รายการผู้ลงเลือกตั้ง</h3>
<table>
    <thead>
        <tr>
            <th>หมายเลขผู้ลงเลือกตั้ง</th>
            <th>ภาพผู้ลงเลือกตั้ง</th>
            <th>ชื่อผู้ลงเลือกตั้ง</th>
            <th>นามสกุลผู้ลงเลือกตั้ง</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['number'] ?></td>
                <td>
                    <img src="<?= url($item['img']) ?>" alt="" style="
                        max-height: 7rem;
                    ">
                </td>
                <td><?= $item['firstname'] ?></td>
                <td><?= $item['lastname'] ?></td>
                <td>
                    <a href="<?= url("/admin/votes/edit-candidate.php?vote={$vote_id}&candidate={$item['candidate_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?vote=<?= $vote_id ?>&action=delete&id=<?= $item['candidate_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบหมายเลข {$item['number']} หรือไม่") ?>
                    >
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขเลือกตั้ง';
require ROOT . '/admin/layout.php';
