<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$vote_id = get('vote');
$candidate_id = get('candidate');
$page_path = "/admin/votes/edit-candidate.php?vote={$vote_id}&candidate={$candidate_id}";

if ($_POST) {
    if (!empty($_FILES['img']['name'])) {
        $img = uplode('img', '/storage/candidate_img');
        if (!$img) {
            setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพผู้ลงเลือกตั้งได้");
            redirect($page_path);
        }

        DB::update('candidates', [
            'img' => $img,
        ], "`candidate_id`='{$candidate_id}'");
    }
    

    $result = DB::update('candidates', [
        'number' => post('number'),
        'firstname' => post('firstname'),
        'lastname' => post('lastname')
    ], "`candidate_id`='{$candidate_id}'");

    if ($result) {
        setAlert('success', "แก้ไขผู้ลงเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขผู้ลงเลือกตั้งได้");
    }
    redirect($page_path);
}

$data = DB::row("SELECT * FROM `candidates` WHERE `candidate_id`='{$candidate_id}'");
ob_start();
?>
<a href="<?= url("/admin/votes/edit.php?vote={$vote_id}") ?>">
    <button>< กลับ</button>
</a>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="img">
        <img src="<?= url($data['img']) ?>" alt="" style="
            max-height: 14rem;
        ">
    </label>
    <br>
    <label for="img">ภาพผู้ลงเลือกตั้ง</label>
    <input type="file" name="img" id="img" accept="image/*">
    <br>

    <label for="number">หมายเลขผู้ลงเลือกตั้ง</label>
    <input type="number" name="number" id="number" value="<?= $data['number'] ?>" required>
    <br>

    <label for="firstname">ชื่อผู้ลงเลือกตั้ง</label>
    <input type="text" name="firstname" id="firstname" value="<?= $data['firstname'] ?>" required>
    <br>

    <label for="lastname">นามสกุลผู้ลงเลือกตั้ง</label>
    <input type="text" name="lastname" id="lastname" value="<?= $data['lastname'] ?>" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขผู้ลงเลือกตั้ง';
require ROOT . '/admin/layout.php';
